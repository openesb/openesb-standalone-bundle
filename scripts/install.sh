#!/bin/sh

## ***************************************************************
## The contents of this file are subject to the terms
## of the Common Development and Distribution License
## (the "License").  You may not use this file except
## in compliance with the License.
## You can obtain a copy of the license at
## www.opensource.org/licenses/CDDL-1.0.
## See the License for the specific language governing
## permissions and limitations under the License.
##
## When distributing Covered Code, include this CDDL
## HEADER in each file and include the License file at
## https://open-esb.dev.java.net/public/CDDLv1.0.html.
## If applicable add the following below this CDDL HEADER,
## with the fields enclosed by brackets "[]" replaced with
## your own identifying information: Portions Copyright
## [year] [name of copyright owner]
##
##
##  Copyright OpenESB Community 2015.
## *****************************************************************

### ====================================================================== ###
##                                                                          ##
##  OpenESB Standalone Setup script                                         ##
##                                                                          ##
### ====================================================================== ###

DIRNAME=`dirname $0`
PROGNAME=`basename $0`

# OS specific support (must be 'true' or 'false').
cygwin=false;
darwin=false;
linux=false;
case "`uname`" in
    CYGWIN*)
        cygwin=true
        ;;

    Darwin*)
        darwin=true
        ;;
        
    Linux)
        linux=true
        ;;
esac

# Force IPv4 on Linux systems since IPv6 doesn't work correctly with jdk5 and lower
if [ "$linux" = "true" ]; then
   JAVA_OPTS="$JAVA_OPTS -Djava.net.preferIPv4Stack=true"
fi

# Setup OPENESB_HOME
if [ "x$OPENESB_HOME" = "x" ]; then
    # get the full path (without any relative bits)
    OPENESB_HOME=`cd $DIRNAME/..; pwd -P`
fi

export OPENESB_HOME
export JAVA_OPTS="$JAVA_OPTS"

# Setup the JVM
if [ "x$JAVA" = "x" ]; then
    if [ "x$JAVA_HOME" != "x" ]; then
    JAVA="$JAVA_HOME/bin/java"
    else
    JAVA="java"
    fi
fi

OPENESB_BIN_PATH=$OPENESB_HOME/bin
OPENESB_COMPONENTS_PATH=$OPENESB_HOME/components
OPENESB_CREDENTIALS="--user admin --passwordfile $OPENESB_HOME/../password.txt"

echo "========================================================================="
echo "=================== Installing OpenESB Shared Libraries ================="
echo "========================================================================="

echo "===================== Shared Library : WSDL Ext"
$OPENESB_BIN_PATH/oeadmin.sh install-jbi-shared-library $OPENESB_CREDENTIALS $OPENESB_COMPONENTS_PATH/wsdlextlib.jar

echo "===================== Shared Library : Encoder"
$OPENESB_BIN_PATH/oeadmin.sh install-jbi-shared-library $OPENESB_CREDENTIALS $OPENESB_COMPONENTS_PATH/encoderlib.jar

echo "========================================================================="
echo "====================== Installing OpenESB Components ===================="
echo "========================================================================="

echo "===================== Service Engine : BPEL"
$OPENESB_BIN_PATH/oeadmin.sh install-jbi-component $OPENESB_CREDENTIALS $OPENESB_COMPONENTS_PATH/bpelse.jar

echo "===================== Binding Component : Database"
$OPENESB_BIN_PATH/oeadmin.sh install-jbi-component $OPENESB_CREDENTIALS $OPENESB_COMPONENTS_PATH/databasebc.jar

echo "===================== Binding Component : File"
$OPENESB_BIN_PATH/oeadmin.sh install-jbi-component $OPENESB_CREDENTIALS $OPENESB_COMPONENTS_PATH/filebc.jar

echo "===================== Binding Component : FTP"
$OPENESB_BIN_PATH/oeadmin.sh install-jbi-component $OPENESB_CREDENTIALS $OPENESB_COMPONENTS_PATH/ftpbc.jar

echo "===================== Binding Component : HTTP"
$OPENESB_BIN_PATH/oeadmin.sh install-jbi-component $OPENESB_CREDENTIALS $OPENESB_COMPONENTS_PATH/httpbc-full.jar

echo "===================== Service Engine : POJO"
$OPENESB_BIN_PATH/oeadmin.sh install-jbi-component $OPENESB_CREDENTIALS $OPENESB_COMPONENTS_PATH/pojose.jar

echo "===================== Binding Component : REST"
$OPENESB_BIN_PATH/oeadmin.sh install-jbi-component $OPENESB_CREDENTIALS $OPENESB_COMPONENTS_PATH/restbc.jar

echo "===================== Binding Component : Email"
$OPENESB_BIN_PATH/oeadmin.sh install-jbi-component $OPENESB_CREDENTIALS $OPENESB_COMPONENTS_PATH/emailbc.jar

echo "===================== Binding Component : JMS"
$OPENESB_BIN_PATH/oeadmin.sh install-jbi-component $OPENESB_CREDENTIALS $OPENESB_COMPONENTS_PATH/jmsbc.jar
